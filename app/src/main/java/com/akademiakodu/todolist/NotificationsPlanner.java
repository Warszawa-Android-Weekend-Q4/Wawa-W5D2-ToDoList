package com.akademiakodu.todolist;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.akademiakodu.todolist.database.ITaskDatabase;
import com.akademiakodu.todolist.model.TodoTask;
import com.akademiakodu.todolist.service.TodoNotificationService;

import java.util.Date;
import java.util.List;

public class NotificationsPlanner {
    private ITaskDatabase mTaskDatabase;
    private Context mContext;

    public NotificationsPlanner(ITaskDatabase taskDatabase, Context context) {
        mTaskDatabase = taskDatabase;
        mContext = context;
    }

    public void planNotifications() {
        // 1. Pobrać zadania, które mają włączone przypomnienie - ale tylko
        // z czasem pozniejszym niz teraz
        List<TodoTask> tasks = mTaskDatabase.getFutureTasksWithReminder(new Date());

        // 2. Dla tych zadań zaplanować za pomocą AlarmManager'a
        // uruchomienie uslugi TodoNotificationService
        AlarmManager alarmManager =
                (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);

        for (TodoTask task : tasks) {
            Intent serviceIntent = new Intent(mContext, TodoNotificationService.class);
            serviceIntent.putExtra("id", task.getId());

            PendingIntent pendingIntent =
                    PendingIntent.getService(mContext, task.getId(),
                            serviceIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                // Dla systemow poniżej API 19 metoda set dziala tak samo
                // jak metoda setExact (ta pojawila sie dopiero w API 19)
                alarmManager.set(AlarmManager.RTC_WAKEUP,
                        task.getReminderDate().getTime(), pendingIntent);
            } else {
                alarmManager.setExact(AlarmManager.RTC_WAKEUP,
                        task.getReminderDate().getTime(), pendingIntent);
            }
        }
    }
}
